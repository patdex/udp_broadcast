from socketserver import UDPServer
import threading
import traceback
import logging
import inspect
import socket
import random
import string
import queue
import json
import time
import sys


# CONSTANTS:

VERSION = 0.2
BROADCAST = '255.255.255.255'
PORT = 50505


class MSG:
    T = 'transmitter'
    R = 'receiver'
    TYPE = 'type'
    VERSION = 'version'
    METHOD = 'method'
    PARAMS = 'params'
    RETURNS = 'returns'
    ERROR = 'error'


class TYPE:
    REQ = 'request'
    RESP = 'response'


class METHOD:
    QUERY = 'query_hostname'
    INSPECT = 'inspect_host'


# specify return dictionary keys so pycharm does not warn
STATUS = 'status'
SUBDICT = 'subdict'
SUBLIST = 'sublist'
P1 = 'param1'
P2 = 'param2'


# LOGGER:

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


# METHODS:

def clear_up(messages: list) -> list:
    """
    clear up message (remove protokoll keys)
    :param messages: message list
    :return:
    """
    cleared = list()

    for message in messages:

        _transmitter = message[MSG.T]

        if MSG.ERROR in message:
            _error = message[MSG.ERROR]
            cleared.append((_transmitter, {MSG.ERROR: _error}))
        else:
            _returns = message[MSG.RETURNS]
            cleared.append((_transmitter, _returns))

    return cleared


# CLASSES:


class DummyWorker:
    """
    Example Worker Class
    """
    def __init__(self):
        self.name = 'Iam a Dummy Class'

    def dummy(self, integer_arg: int=3, float_arg: float=0.1, string_arg: str='string', list_arg: list=tuple()) \
            -> {STATUS: True, SUBDICT: {P1: 1, P2: 2.0}, SUBLIST: [P1, P2]}:

        return {STATUS: True,
                SUBDICT: {P1: integer_arg,
                          P2: float_arg},
                SUBLIST: list_arg.extend([string_arg, P2, self.name])}


class ClassEncoder(json.JSONEncoder):
    """
    inspect class to generate json spec
    """

    def default(self, o):

        methods_spec = list()
        methods = inspect.getmembers(o, inspect.ismethod)
        for (name, method) in methods:
            if name.startswith('_'):
                continue
            methods_spec.append(self._inspect_method(method))

        return methods_spec

    @classmethod
    def _inspect_method(cls, func):
        """
        inspect method
        :param func:
        :return:
        """
        method_spec = dict()
        params_spec = dict()
        method_spec['name'] = func.__name__

        signature = inspect.signature(func)
        for name, value in signature.parameters.items():
            params_spec[name] = value.default
        if params_spec:
            method_spec['params'] = params_spec

        rano = signature.return_annotation
        if isinstance(rano, dict) or isinstance(rano, list):
            method_spec['returns'] = rano

        return method_spec


class UDPBroadcastServer(UDPServer):
    """
    udp broadcast server
    - standard methods (query_hostname, inspect_host)
    - takes worker class, instantiate, inspect methods
    - send broadcast messages
    - saves responses
    - assign saved responses to request call
    """

    def __init__(self, *args, **kwargs):
        self.identifier = kwargs.pop('identifier')  # hopefully unique address for this server
        worker = kwargs.pop('worker')  # get worker class
        super().__init__(*args, **kwargs)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)  # allow send to broadcast address
        self.resp = queue.Queue()  # collected responses
        self.stop = False  # variable to stop server thread

        # initialize worker
        self.worker = worker() if worker is not None else DummyWorker()

        # inspect:
        self.spec = json.loads(json.dumps(cls=ClassEncoder, obj=self.worker))  # generate spec for worker class
        self.methods = self.inspect_worker_methods_to_dict()  # keys: method names, values: method references
        self.methods.update({METHOD.QUERY: self.query_hostname, METHOD.INSPECT: self.inspect_host})  # + default methods

    def inspect_worker_methods_to_dict(self):
        """
        inspect worker methods and return a dict with function name and function reference
        :return:
        """
        _methods = dict()
        for (name, method) in inspect.getmembers(self.worker, inspect.ismethod):
            _methods[name] = method
        return _methods

    def send_via_broadcast(self, receiver: str, data: dict, request=True):
        """
        send broadcast message
        :param receiver: receiver address
        :param data: message
        :param request: request = True, response = False
        :return:
        """

        # add required arguments to message:
        data[MSG.R] = receiver
        data[MSG.T] = self.identifier
        data[MSG.VERSION] = VERSION
        data[MSG.TYPE] = TYPE.REQ if request else TYPE.RESP

        # format and send message to broadcast address:
        json_str = json.dumps(data)
        self.socket.sendto(json_str.encode('utf-8'), (BROADCAST, PORT))
        logger.info('OUTGOING: ' + json_str)

    def receive_method_responses(self, method: str, num=None, timeout=1.0):
        """
        get responses for given method, throw away other messages
        :param method: called method
        :param num:
        :param timeout:
        :return:
        """
        responses = list()
        start = time.time()

        while True:

            if self.resp.empty():
                time.sleep(0.1)
            else:
                msg = self.resp.get(block=True, timeout=0.1)
                if msg[MSG.METHOD] == method:
                        responses.append(msg)

            if len(responses) == num:
                break
            if time.time() - start > timeout:
                break

        return responses

    # DEFAULT METHODS:

    @classmethod
    def query_hostname(cls):
        return {'hostname': socket.gethostname()}

    def inspect_host(self):
        return {'spec.json': self.spec}


class BroadcastHandler:
    """
    handle broadcast traffic
    - check addresses
    - execute incomming requests
    - store responses to server
    """

    def __init__(self, request, client_address, server: UDPBroadcastServer):
        self.request = request
        self.client_address = client_address
        self.server = server

        # parse incomming json string:
        try:
            self.message = json.loads(request[0].decode('utf-8'))
        except Exception as e:
            logger.error('to json error: ' + str(e))
            logger.error(traceback.format_exc())

        # check if message for this server:
        try:
            if not self._for_me():
                logger.debug('IGNORE: ' + str(self.message))
                return
        except Exception as e:
            logger.error('for me error: ' + str(e))
            logger.error(traceback.format_exc())

        # handle message:
        try:
            if self.message[MSG.TYPE] == TYPE.REQ:
                self._handle_request()

            elif self.message[MSG.TYPE] == TYPE.RESP:
                self._handle_response()

        except Exception as e:
            logger.error('handle error: ' + str(e))
            logger.error(traceback.format_exc())

        finally:
            self._finish()

    def _for_me(self) -> bool:
        """
        is message addressed to this server
        :return:
        """
        _receiver = self.message[MSG.R]
        _transmitter = self.message[MSG.T]

        if _transmitter == self.server.identifier:  # self transmitted message
            return False

        if _receiver == '*':  # wildcard
            return True

        elif '*' in _receiver:  # address beginning + wildcard
            _addr = _receiver.split('*')[0]
            if self.server.identifier.startswith(_addr):
                return True

        else:  # fully address (no wildcard)
            if _receiver == self.server.identifier:
                return True

        return False

    def _handle_request(self):
        """
        handle for this server addressed requests
        """
        logger.info('INCOMING REQUEST: ' + str(self.message))

        # try to run method
        try:
            method = self.server.methods[self.message[MSG.METHOD]]
            params = self.message.pop(MSG.PARAMS) if MSG.PARAMS in self.message else {}
            self.message[MSG.RETURNS] = method(**params)

        except Exception as e:
            logger.error(traceback.format_exc())
            self.message[MSG.ERROR] = str(e)

        # send message
        self.server.send_via_broadcast(self.message[MSG.T], self.message, request=False)

    def _handle_response(self):
        """
        handle for this server addressd responses
        :return:
        """
        logger.info('INCOMING RESPONSE: ' + str(self.message))
        self.server.resp.put(self.message)

    def _finish(self):
        pass


class UDPBroadcast:

    def __init__(self,
                 interface_ip: str= '0.0.0.0',
                 identifier: str= 'ba:de:00:00:af:fe',
                 worker=None,
                 append_random=True):

        # generate random append string
        if append_random:
            ident_append = '::' + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
        else:
            ident_append = ''

        # configure and run server
        self._server = UDPBroadcastServer((interface_ip, PORT),
                                          BroadcastHandler,
                                          identifier=identifier + ident_append,
                                          worker=worker)

        self._server_thread = threading.Thread(target=self._server.serve_forever)
        self._server_thread.start()

    def generic(self, method, params=None, address: str='*', num=None, timeout=1.0):
        """
        generic caller
        :param address: destination address
        :param method: method name
        :param params: params dict
        :param num: number of expected answers
        :param timeout: timout for method call in seconds
        :return:
        """
        if params:
            self._server.send_via_broadcast(address, {MSG.METHOD: method, MSG.PARAMS: params})
        else:
            self._server.send_via_broadcast(address, {MSG.METHOD: method})

        messages = self._server.receive_method_responses(method=method, num=num, timeout=timeout)
        return clear_up(messages)

    def query_hostnames(self, timeout=3.0):
        """
        standard method to query all running udp broadcast server hostnames in subnet
        :return:
        """
        return self.query_hostname('*', num=None, timeout=timeout)

    def query_hostname(self, address: str='*', num=None, timeout=3.0):
        """
        standard method to query running udp broadcast server hostname
        :param address:
        :param num:
        :param timeout:
        :return:
        """
        self._server.send_via_broadcast(address, {MSG.METHOD: METHOD.QUERY})
        messages = self._server.receive_method_responses(method=METHOD.QUERY, num=num, timeout=timeout)
        return clear_up(messages)

    def inspect_host(self, address: str='*', num=None, timeout=3.0):
        """
        query host methods (spec.json)
        :return:
        """
        self._server.send_via_broadcast(address, {MSG.METHOD: METHOD.INSPECT})
        messages = self._server.receive_method_responses(method=METHOD.INSPECT, num=num, timeout=timeout)
        return clear_up(messages)

    def close(self):
        """
        shut down the server
        :return:
        """
        if self._server_thread.is_alive():
            logger.info('shutdown server')
            self._server.shutdown()
            self._server.socket.close()
            logger.info('server is down')

    def __del__(self):
        """
        cleanup server on delete
        :return:
        """
        self.close()


if __name__ == '__main__':

    u = UDPBroadcast(identifier='TESTER', append_random=False)

    hosts = u.query_hostname('*')
    for transmitter, returns in hosts:
        print((transmitter, returns))

    if hosts:
        for _, returns in u.inspect_host(hosts[0][0], num=1):
            if returns:
                print(json.dumps(returns['spec.json'], indent=4, sort_keys=True))

    if hosts:
        for addr, returns in u.generic(address=hosts[0][0], method='get_info', params={}, num=1):

            print('\n' + addr)
            for key, value in returns.items():
                print('\t{key}: {value}'.format(key=key, value=value))
            print()

    if hosts:
        for addr, returns in u.generic(address=hosts[0][0], method='set_ip', params={'ip': '10.102.4.213', 'netmask': '255.255.255.0'}, num=1, timeout=5):

            print(addr)
            print('\t' + str(returns))

    u.close()
